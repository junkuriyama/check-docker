
image:
	docker build -t kuriyama/check-docker -f Dockerfile .

pull:
	@docker pull python:3-alpine > .log
	@docker image inspect python:3-alpine | jq -r '.[0].Id' > .id

check:
	@if git status -s | grep -q M; then\
		git add .etag .id && git commit -m "Update IDs."; git push;\
	fi
etag:
	@curl -s https://pypi.org/pypi/check-docker/json | jq -r .info.version > .etag
